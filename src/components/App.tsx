import React, { useEffect, useRef, useState } from 'react';
import Pins from '../pins';
import { TimerBar } from './TimerBar';
import { Timer } from './Timer';
import { SelectedPin } from './SelectedPin';
import { PinProgress } from './PinProgress';
import { Keypad } from './Keypad';
import { CorrectPins } from './CorrectPins';
import { Rules } from './Rules';
import { Replay } from './Replay';
import { Win } from './Win';
import { DigitModel } from '../models';
import './App.css';

const App = () => {
  const defaultPin: DigitModel[] = [
    { digit: '?', status: 'none' },
    { digit: '?', status: 'none' },
    { digit: '?', status: 'none' },
    { digit: '?', status: 'none' }
  ];
  const [showTimer, setShowTimer] = useState(true);
  const [remainingTime, setRemainingTime] = useState('');
  const [pinProgress, setPinProgress] = useState(defaultPin as DigitModel[]);
  const [enteredPin, setEnteredPin] = useState([] as DigitModel[]);
  const [correctPin, setCorrectPin] = useState('');
  const [correctGuesses, setCorrectGuesses] = useState(0);
  const [resetPIN, setResetPIN] = useState('');
  const [disableButtons, setDisableButtons] = useState(false);
  const [showReplay, setShowReplay] = useState(false);
  const [showWin, setShowWin] = useState(false);
  const requiredGuesses = useRef(1);
  let timerCalled = useRef(false);
  let interval = useRef<any>();

  const resetTimer = () => {
    setRemainingTime('');
    const date = new Date();
    date.setMinutes(date.getMinutes() + 1);

    interval.current = setInterval(() => {
      const now = new Date().getTime();
      const distance = date.getTime() - now;

      const seconds = Math.floor((distance % (1000 * 60)) / 1000);
      const miliseconds = Math.floor((distance % (1000))).toString().substring(0, 2);
      const addZero = seconds < 10;

      setRemainingTime(`00:${addZero ? `0${seconds}` : seconds}:${miliseconds.length < 2 ? `${miliseconds}0` : miliseconds}`);

      if (distance < 0) {
        clearInterval(interval.current);
        setPinProgress(defaultPin);
        setEnteredPin([]);
        changePin();
        setShowTimer(false);
        setDisableButtons(true);
        setShowReplay(true);
      }
    }, 10);
  };

  useEffect(() => {
    let pinMatches = true;
    pinProgress.forEach(d => {
      if (d.status === 'misplaced' || d.status === 'none') {
        pinMatches = false;
      }
    });

    if (pinMatches) {
      console.info('PIN complete, loading another...');

      if (correctGuesses + 1 === requiredGuesses.current) {
        clearInterval(interval.current);
        setShowTimer(false);
        setCorrectGuesses(correctGuesses + 1);
        setShowWin(true);
      } else {
        setCorrectGuesses(correctGuesses + 1);
        setPinProgress(defaultPin);
        changePin();
      }
    }

    setEnteredPin([]);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [resetPIN])

  useEffect(() => {
    if (enteredPin.length === 4) {
      setPinProgress(enteredPin);
      const correctPinArray = correctPin.split('');
      const newArray = [...enteredPin];

      enteredPin.forEach((a, index) => {
        const correctPinIndex = correctPinArray.indexOf(a.digit);

        if (correctPinIndex === index) {
          newArray.splice(index, 1, { digit: a.digit, status: 'match' });
          setPinProgress(newArray);
        } else if (correctPinIndex > -1) {
          newArray.splice(index, 1, { digit: a.digit, status: 'misplaced' });
          setPinProgress(newArray);
        }
      });
    }
  }, [correctPin, enteredPin]);

  const changePin = () => setCorrectPin(Pins[Math.floor(Math.random() * Pins.length)]);
  useEffect(() => changePin(), []);

  return (
    <div className="App">
      <TimerBar
        remainingTime={remainingTime}
        resetTimer={resetTimer}
        timerCalled={timerCalled}
      />
      <Timer
        remainingTime={remainingTime}
        resetTimer={resetTimer}
        timerCalled={timerCalled}
        showTimer={showTimer}
      />
      <PinProgress
        enteredPin={enteredPin}
      />
      <Keypad
        enteredPin={enteredPin}
        setEnteredPin={setEnteredPin}
        setResetPIN={setResetPIN}
        disableButtons={disableButtons}
      />
      <SelectedPin
        pin={correctPin}
        pinProgress={pinProgress}
      />
      <CorrectPins
        correctGuesses={correctGuesses}
        requiredGuesses={requiredGuesses}
      />
      <Rules />
      {
        showReplay &&
        <Replay
          setShowReplay={setShowReplay}
          setShowTimer={setShowTimer}
          setDisableButtons={setDisableButtons}
          resetTimer={resetTimer}
        />
      }
      {
        showWin &&
        <Win
          setShowWin={setShowWin}
          setShowTimer={setShowTimer}
          setDisableButtons={setDisableButtons}
          defaultPin={defaultPin}
          setPinProgress={setPinProgress}
          setCorrectGuesses={setCorrectGuesses}
          changePin={changePin}
          resetTimer={resetTimer}
        />
      }
    </div>
  );
}

export default App;
