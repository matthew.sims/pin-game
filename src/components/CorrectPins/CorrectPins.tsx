import React from 'react';
import './CorrectPins.css';

export const CorrectPins = (
  {
    correctGuesses,
    requiredGuesses
  }:
  {
    correctGuesses: number;
    requiredGuesses: React.MutableRefObject<number>;
  }) => {
  return (
    <div className="correct-pins">
      {
        correctGuesses !== requiredGuesses.current
        ? <>
            PINs Bypassed <br /> { correctGuesses } of { requiredGuesses.current }
          </>
        : <>
            All PINS solved!
          </>
      }
    </div>
  );
}
