import React, { useEffect } from 'react';
import { DigitModel } from '../../models';
import './Keypad.css';

export const Keypad = (
  {
    enteredPin,
    setEnteredPin,
    setResetPIN,
    disableButtons
  }:
  {
    enteredPin: DigitModel[];
    setEnteredPin: React.Dispatch<DigitModel[]>;
    setResetPIN: React.Dispatch<any>;
    disableButtons: boolean;
  }) => {
  const handleKeypadPress = (digit: string) => {
    setEnteredPin([...enteredPin, { digit, status: 'none' }]);
  };

  useEffect(() => {
    if (enteredPin.length === 4) {
      const randomNumber = Math.floor(Math.random() * 9999999);
      const buttons = document.querySelectorAll('.keypad button') as NodeListOf<HTMLButtonElement>;
      buttons.forEach(button => {
        button.disabled = true;
      });

      const timeout = setTimeout(() => {
        clearTimeout(timeout);
        setResetPIN(randomNumber.toString());
        buttons.forEach(button => {
          button.disabled = false;
        });
      }, 3000);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [enteredPin, setEnteredPin])

  return (
    <div className="keypad">
      <button onClick={() => handleKeypadPress('1')} disabled={disableButtons}>1</button>
      <button onClick={() => handleKeypadPress('2')} disabled={disableButtons}>2</button>
      <button onClick={() => handleKeypadPress('3')} disabled={disableButtons}>3</button>
      <button onClick={() => handleKeypadPress('4')} disabled={disableButtons}>4</button>
      <button onClick={() => handleKeypadPress('5')} disabled={disableButtons}>5</button>
      <button onClick={() => handleKeypadPress('6')} disabled={disableButtons}>6</button>
      <button onClick={() => handleKeypadPress('7')} disabled={disableButtons}>7</button>
      <button onClick={() => handleKeypadPress('8')} disabled={disableButtons}>8</button>
      <button onClick={() => handleKeypadPress('9')} disabled={disableButtons}>9</button>
      <span>*</span>
      <button onClick={() => handleKeypadPress('0')} disabled={disableButtons}>0</button>
      <span>#</span>
    </div>
  );
}
