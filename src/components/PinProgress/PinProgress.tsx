import React from 'react';
import { DigitModel } from '../../models';
import './PinProgress.css';

export const PinProgress = (
  {
    enteredPin
  }:
  {
    enteredPin: DigitModel[]
  }) => {

  return (
    <div className="pin-progress">
      {
        enteredPin &&
        enteredPin.map((pin, index) => <span key={index}>
          { pin.status === 'match' && <span className='match'>{ pin.digit }</span> }
          { pin.status === 'misplaced' && <span className='misplaced'>{ pin.digit }</span> }
          { pin.status === 'none' && <span className='none'>{ pin.digit }</span> }
        </span>)
      }
    </div>
  );
}
