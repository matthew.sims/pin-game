import React from 'react';
import './Replay.css';

export const Replay = (
  {
    setShowReplay,
    setShowTimer,
    setDisableButtons,
    resetTimer
  }:
  {
    setShowReplay: React.Dispatch<boolean>;
    setShowTimer: React.Dispatch<boolean>;
    setDisableButtons: React.Dispatch<boolean>;
    resetTimer: () => void;
  }
) => {
  const handleReplay = () => {
    setShowReplay(false);
    setShowTimer(true);
    setDisableButtons(false);
    resetTimer();
  };

  return (
    <div className='replay'>
      <div className="modal">
        <h1>Mission Failed!</h1>
        <p>You failed to bypass enough PINs before the system triggered the alarm.</p>

        <button onClick={() => handleReplay()}>Try Again?</button>
      </div>
    </div>
  );
}
