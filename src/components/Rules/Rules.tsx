import React from 'react';
import './Rules.css';

export const Rules = () => {
  return (
    <div className='rules'>
      <p>Your USB contains a virus that can open the vault, but there are security PINs that the virus cannot bypass on its own. Find and bypass the PINs manually before the system triggers an alarm!</p>
      <div className='definitions'>
        <p>
          <span className='green'>Green Numbers:</span> Correct number, correct position
        </p>
        <p>
          <span className='orange'>Orange Numbers:</span> Correct number, wrong position
        </p>
        <p>
          <span>Pale Numbers:</span> Wrong number
        </p>
      </div>
    </div>
  );
}
