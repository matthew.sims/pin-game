import React from 'react';
import { DigitModel } from '../../models';
import './SelectedPin.css';

export const SelectedPin = (
  {
    pin,
    pinProgress,
  }:
  {
    pin: string,
    pinProgress: DigitModel[];
  }) => {
  return (
    <div className="selected-pin">
      { pinProgress && pinProgress.map((pin, index) => <span key={index}>
          { pin.status === 'match' && <span className='match'>{ pin.digit }</span> }
          { pin.status === 'misplaced' && <span className='misplaced'>{ pin.digit }</span> }
          { pin.status === 'none' && <span className='none'>{ pin.digit }</span> }
        </span>) }
    </div>
  );
}
