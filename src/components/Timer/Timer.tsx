import React, { useEffect } from 'react';
import './Timer.css';

export const Timer = (
  {
    remainingTime,
    resetTimer,
    timerCalled,
    showTimer
  }:
  {
    remainingTime: string;
    resetTimer: () => void;
    timerCalled: any;
    showTimer: boolean;
  }) => {
  const timeParts = remainingTime.split(':');
  useEffect(() => {
    if (timerCalled.current === false) {
      timerCalled.current = true;
      resetTimer();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <div className="timer">
      { showTimer && timeParts?.length > 1 && timeParts.map((part, index) => (
          index === 1 ? <span key={index}>:{ Number(timeParts[1]) <= 10 ? <span className='low-time'>{ part }</span> : part }:</span> 
          : part
        ))
      }
      {
        !showTimer &&
        <>
          00<span>:00:</span>00
        </>
      }
    </div>
  );
}
