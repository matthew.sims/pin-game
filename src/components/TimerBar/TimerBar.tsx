import React, { useEffect } from 'react';
import './TimerBar.css';

export const TimerBar = (
  {
    remainingTime,
    resetTimer,
    timerCalled
  }:
  {
    remainingTime: string;
    resetTimer: () => void;
    timerCalled: any;
  }) => {
  const timeParts = remainingTime.split(':');
  const totalSeconds = (Number(timeParts[0]) * 60) + Number(timeParts[1]);

  useEffect(() => {
    if (timerCalled.current === false) {
      timerCalled.current = true;
      resetTimer();
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <div className="timer-bar">
      { totalSeconds > 0 &&
        <div className={`bar ${ totalSeconds < 11 && 'red-time' } `} style={{ width: `${(100 / 60) * totalSeconds}%` }}></div>
      }
    </div>
  );
}
