import React from 'react';
import './Win.css';
import { DigitModel } from '../../models';

export const Win = (
  {
    setShowWin,
    setShowTimer,
    setDisableButtons,
    defaultPin,
    setPinProgress,
    setCorrectGuesses,
    changePin,
    resetTimer
  }:
  {
    setShowWin: React.Dispatch<boolean>;
    setShowTimer: React.Dispatch<boolean>;
    setDisableButtons: React.Dispatch<boolean>;
    defaultPin: DigitModel[];
    setPinProgress: React.Dispatch<DigitModel[]>;
    setCorrectGuesses: React.Dispatch<number>;
    changePin: () => void;
    resetTimer: () => void;
  }
) => {
  const handleReplay = () => {
    setShowWin(false);
    setShowTimer(true);
    setDisableButtons(false);
    setPinProgress(defaultPin);
    setCorrectGuesses(0);
    changePin();
    resetTimer();
  };

  return (
    <div className='win'>
      <div className="modal">
        <h1>Success!</h1>
        <p>You were able to successfully bypass the PINs before the system triggered an alarm!</p>

        <button onClick={() => handleReplay()}>Play Again?</button>
      </div>
    </div>
  );
}
