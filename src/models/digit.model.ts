export interface DigitModel {
  digit: string;
  status: 'match' | 'misplaced' | 'none'
}
